package co.com.onoff.pruebaingreso.api;

import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET(EndPoints.GET_POST)
    Call<JsonArray> getPost();
}
