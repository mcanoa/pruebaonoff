package co.com.onoff.pruebaingreso.model;

public interface CommentInteractor {

    void getComments(String id);
    void saveComment(Comment comment);

}
