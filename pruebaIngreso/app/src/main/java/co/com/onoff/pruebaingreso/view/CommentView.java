package co.com.onoff.pruebaingreso.view;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Comment;

public interface CommentView {

    void getComments();
    void saveComment(Comment comment);
    void  showComments(ArrayList<Comment> comments);
}
