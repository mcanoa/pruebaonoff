package co.com.onoff.pruebaingreso.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.R;
import co.com.onoff.pruebaingreso.model.Post;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {


    private final ArrayList<Post> listPostsUser;
    private final int resource;

    public PostAdapter(ArrayList<Post> listPostsUser, int resource){
        this.listPostsUser=listPostsUser;
        this.resource= resource;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Post post= listPostsUser.get(position);
        holder.setData(post);
    }

    @Override
    public int getItemCount() {
        return listPostsUser.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        Post post;
        TextView title;
        TextView body;
        public ViewHolder(View itemView) {
            super(itemView);

            title= itemView.findViewById(R.id.title);
            body= itemView.findViewById(R.id.body);
            itemView.setOnClickListener(this);
        }

        public void setData(Post post){
            this.post=post;

            title.setText(post.getTittle());
            body.setText(post.getBody());
        }

        @Override
        public void onClick(View v) {

            Context context= v.getContext();
            Intent intent= new Intent(context,PostDetailActivity.class);
            intent.putExtra("POST", post);
            context.startActivity(intent);
        }
    }
}
