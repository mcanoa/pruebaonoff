package co.com.onoff.pruebaingreso.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import co.com.onoff.pruebaingreso.presenter.CommentPresenter;

public class CommentRepositoryImpl implements CommentRepository {

    private CommentPresenter commentPresenter;

    public CommentRepositoryImpl(CommentPresenter commentPresenter) {
        this.commentPresenter = commentPresenter;
    }

    @Override
    public void getCommentsDatabae(String id) {

        ArrayList<Comment> comments= new ArrayList<>();
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query="SELECT comment, idPost FROM Comment WHERE idPost='"+id+"'";

            Log.v("QUERY",query);

            Cursor cursor= db.rawQuery(query,null);

            if(cursor.moveToFirst()){
                while (cursor.moveToNext()) {
                    String comment= cursor.getString(cursor.getColumnIndex("comment"));
                    String idPost= cursor.getString(cursor.getColumnIndex("idPost"));

                    comments.add(new Comment(comment,idPost));
                }
            }

            cursor.close();

        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarComment" + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
        commentPresenter.showComments(comments);
    }

    @Override
    public void saveComment(Comment comment) {

        ContentValues values;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            values = new ContentValues();

            values.put("comment", comment.getComment());
            values.put("idPost", comment.getIdPost());

            db.insertOrThrow("Comment", null, values);

        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarComment" + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

    }


    private static File dirApp() {

        File SDCardRoot = Environment.getExternalStorageDirectory(); //Environment.getDataDirectory(); //
        File dirApp = new File(SDCardRoot.getPath() + "/PruebaIngreso");

        if (!dirApp.isDirectory())
            dirApp.mkdirs();

        return dirApp;
    }
}
