package co.com.onoff.pruebaingreso.presenter;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Post;
import co.com.onoff.pruebaingreso.model.PostInteractor;
import co.com.onoff.pruebaingreso.model.PostInteractorImpl;
import co.com.onoff.pruebaingreso.view.PostView;

public class PostPresenterImpl implements PostPresenter{

    private final PostView postView;
    private  final PostInteractor postInteractor;



    public  PostPresenterImpl(PostView postView){
        this.postView= postView;
        postInteractor= new PostInteractorImpl(this);
    }

    @Override
    public void getPost() {
        postInteractor.getPost();
    }

    @Override
    public void getPostBy(String title) {
        postInteractor.getPostBy(title);
    }

    @Override
    public void showPost(ArrayList<Post> posts) {

        postView.showPost(posts);
    }
}
