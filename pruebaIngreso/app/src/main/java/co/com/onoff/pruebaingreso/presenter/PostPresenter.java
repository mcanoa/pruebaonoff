package co.com.onoff.pruebaingreso.presenter;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Post;

public interface PostPresenter {

    void getPost();
    void getPostBy(String title);
    void showPost(ArrayList<Post> posts);
}
