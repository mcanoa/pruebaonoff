package co.com.onoff.pruebaingreso.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.R;
import co.com.onoff.pruebaingreso.model.Post;
import co.com.onoff.pruebaingreso.presenter.PostPresenter;
import co.com.onoff.pruebaingreso.presenter.PostPresenterImpl;

public class MainActivity extends AppCompatActivity implements PostView{

    RecyclerView recyclerViewPost;
    EditText editTextSearch;
    PostPresenter postPresenter;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                }

            } else {
                init();
            }
        } else {
            init();
        }

    }

    private void init(){
        postPresenter= new PostPresenterImpl(MainActivity.this);
        recyclerViewPost= findViewById(R.id.recyclerViewPost);
        recyclerViewPost.setLayoutManager(new LinearLayoutManager(this));
        editTextSearch=findViewById(R.id.editTextSearch);
        filterPost();

        getPost();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                init();

            } else {

                finish();
            }
            return;
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void filterPost(){
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(!s.toString().trim().isEmpty()) {
                    postPresenter.getPostBy(s.toString());
                }else{
                    getPost();
                }
            }
        });
    }

    @Override
    public void getPost() {

        ProgressView.getInstance().Show(MainActivity.this,"Cargando...");
        postPresenter.getPost();
    }

    @Override
    public void showPost(ArrayList<Post> posts) {

        PostAdapter adapter= new PostAdapter(posts,R.layout.post_item);
        recyclerViewPost.setAdapter(adapter);
        ProgressView.getInstance().Dismiss();
    }
}