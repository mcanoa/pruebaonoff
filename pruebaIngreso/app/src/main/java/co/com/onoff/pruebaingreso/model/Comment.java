package co.com.onoff.pruebaingreso.model;

public class Comment {

    private String comment;
    private String idPost;

    public Comment( String comment, String idPost) {
        this.comment = comment;
        this.idPost = idPost;
    }

    public String getComment() {
        return comment;
    }

    public String getIdPost() {
        return idPost;
    }
}
