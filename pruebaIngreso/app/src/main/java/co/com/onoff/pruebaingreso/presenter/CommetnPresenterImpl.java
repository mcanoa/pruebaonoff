package co.com.onoff.pruebaingreso.presenter;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Comment;
import co.com.onoff.pruebaingreso.model.CommentInteractor;
import co.com.onoff.pruebaingreso.model.CommentInteractorImpl;
import co.com.onoff.pruebaingreso.view.CommentView;

public class CommetnPresenterImpl implements CommentPresenter {

    private CommentView commentView;
    private CommentInteractor commentInteractor;

    public CommetnPresenterImpl(CommentView commentView) {

        this.commentView = commentView;
        commentInteractor = new CommentInteractorImpl(this);
    }

    @Override
    public void getComments(String id) {
        commentInteractor.getComments(id);

    }

    @Override
    public void saveComment(Comment comment) {
        commentInteractor.saveComment(comment);
    }

    @Override
    public void showComments(ArrayList<Comment> comments) {
        commentView.showComments(comments);
    }
}
