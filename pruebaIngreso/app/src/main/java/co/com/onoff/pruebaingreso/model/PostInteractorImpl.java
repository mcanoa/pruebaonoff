package co.com.onoff.pruebaingreso.model;


import co.com.onoff.pruebaingreso.presenter.PostPresenter;

public class PostInteractorImpl implements PostInteractor{

    private final PostRepository postRepository;

    public PostInteractorImpl(PostPresenter postPresenter){

        postRepository = new PostRepositoryImpl(postPresenter);
    }

    @Override
    public void getPost() {

        if(postRepository.validateDataOnDatabase()){
            postRepository.getPostDatabase();
        }else{
            postRepository.getPostAPI();
        }

    }

    @Override
    public void getPostBy(String title) {
        postRepository.getPostByTitle(title);
    }
}
