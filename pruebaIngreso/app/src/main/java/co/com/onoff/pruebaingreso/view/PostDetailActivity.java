package co.com.onoff.pruebaingreso.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.R;
import co.com.onoff.pruebaingreso.model.Comment;
import co.com.onoff.pruebaingreso.model.Post;
import co.com.onoff.pruebaingreso.presenter.CommentPresenter;
import co.com.onoff.pruebaingreso.presenter.CommetnPresenterImpl;

public class PostDetailActivity extends AppCompatActivity implements View.OnClickListener, CommentView{


    private Post post;
    private CommentPresenter commentPresenter;
    private RecyclerView recyclerViewComment;
    private EditText editTextComment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        post=(Post) getIntent().getSerializableExtra("POST");

        editTextComment= findViewById(R.id.editTextComment);
        TextView titlePost= findViewById(R.id.title_post);
        TextView contentPost= findViewById(R.id.content_post);
        recyclerViewComment= findViewById(R.id.recyclerViewComment);
        recyclerViewComment.setLayoutManager(new LinearLayoutManager(this));

        Button addComment=findViewById(R.id.buttonAddComment);
        addComment.setOnClickListener(this);

        titlePost.setText(post.getTittle());
        contentPost.setText(post.getBody());

        commentPresenter= new CommetnPresenterImpl(PostDetailActivity.this);
        getComments();

    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.buttonAddComment) {
            String textComment = editTextComment.getText().toString();
            Comment newComment = new Comment(textComment, post.getId());

            saveComment(newComment);
            CommentAdapter commentAdapter= (CommentAdapter) recyclerViewComment.getAdapter();
            if(commentAdapter!=null) {
                commentAdapter.updateAdapter(newComment);
            }
            editTextComment.setText("");

        }
    }

    @Override
    public void getComments() {

        commentPresenter.getComments(post.getId());
    }

    @Override
    public void saveComment(Comment comment) {
        commentPresenter.saveComment(comment);
    }

    @Override
    public void showComments(ArrayList<Comment> comments) {
        CommentAdapter adapter= new CommentAdapter(comments,R.layout.comment_item);
        recyclerViewComment.setAdapter(adapter);
    }
}