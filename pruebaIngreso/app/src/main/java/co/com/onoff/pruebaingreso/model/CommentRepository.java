package co.com.onoff.pruebaingreso.model;

public interface CommentRepository {

    void getCommentsDatabae(String id);
    void saveComment(Comment comment);
}
