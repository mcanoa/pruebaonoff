package co.com.onoff.pruebaingreso.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import co.com.onoff.pruebaingreso.api.ApiAdapter;
import co.com.onoff.pruebaingreso.api.ApiService;
import co.com.onoff.pruebaingreso.presenter.PostPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostRepositoryImpl implements PostRepository {

    private final PostPresenter postPresenter;
    public PostRepositoryImpl(PostPresenter postPresenter){
        this.postPresenter= postPresenter;
    }

    @Override
    public void getPostAPI() {

        final ArrayList<Post> posts = new ArrayList<>();

        ApiAdapter apiAdapter = new ApiAdapter();
        ApiService apiService = apiAdapter.getUsersService();
        Call<JsonArray> call = apiService.getPost();

        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                if (response.body() != null) {
                    JsonArray postJson = response.body().getAsJsonArray();
                    Log.v("INFORMACION", postJson.toString());

                    for (JsonElement infoJson : postJson) {
                        JsonObject postInfo = infoJson.getAsJsonObject();
                        Post post= new Post(postInfo);
                        posts.add(post);
                    }


                    createDatabase();
                    savePost(posts);

                    postPresenter.showPost(posts);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                Log.e("ERROR", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    @Override
    public void savePost(ArrayList<Post> posts) {

        ContentValues values;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            values = new ContentValues();

            for(Post post:posts){

                values.put("id",post.getId());
                values.put("title",post.getTittle());
                values.put("body",post.getBody());

                db.insertOrThrow("Post",null,values);
            }

        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarPost " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

    }

    @Override
    public void getPostDatabase() {

        ArrayList<Post> posts=new ArrayList<>();
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query="SELECT id, title, body FROM Post";

            Cursor cursor= db.rawQuery(query,null);

            if(cursor.moveToFirst()){
                while (cursor.moveToNext()){
                    String id= cursor.getString(cursor.getColumnIndex("id"));
                    String title= cursor.getString(cursor.getColumnIndex("title"));
                    String body= cursor.getString(cursor.getColumnIndex("body"));

                    posts.add(new Post(id,title,body));
                }

            }

            cursor.close();
        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarPost " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        postPresenter.showPost(posts);
    }

    /**
     * Metodo para validar si existe informacion en la base de datos sobre los post
     * @return true si hay informacion o false de lo contrario
     */
    @Override
    public boolean validateDataOnDatabase() {

        boolean hayInfo=false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query="SELECT COUNT(*) AS registros FROM Post";

            Cursor cursor= db.rawQuery(query,null);

            if(cursor.moveToFirst()){
                int cantidadRegistros= cursor.getInt(cursor.getColumnIndex("registros"));
                if(cantidadRegistros>0){
                    hayInfo=true;
                }

            }

            cursor.close();
        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarPost " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return hayInfo;
    }

    @Override
    public void getPostByTitle(String title) {
        ArrayList<Post> posts=new ArrayList<>();
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query="SELECT id, title, body FROM Post WHERE title LIKE '%"+title+"%'";

            Cursor cursor= db.rawQuery(query,null);

            if(cursor.moveToFirst()){
                while (cursor.moveToNext()){
                    String id= cursor.getString(cursor.getColumnIndex("id"));
                    String titlePost= cursor.getString(cursor.getColumnIndex("title"));
                    String body= cursor.getString(cursor.getColumnIndex("body"));

                    posts.add(new Post(id,titlePost,body));
                }

            }

            cursor.close();
        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("DataBaseBO", "RegistrarPost " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        postPresenter.showPost(posts);
    }

    private void createDatabase(){
        SQLiteDatabase db = null;
        String config;

        try {

            File dbFile = new File(dirApp(), "Database.db");
            db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);

            config = "CREATE TABLE IF NOT EXISTS Post(id varchar(255), title varchar(255), body varchar(255))";
            db.execSQL(config);

            config = "CREATE TABLE IF NOT EXISTS Comment(comment varchar(255), idPost varchar(10))";
            db.execSQL(config);



        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e("Error", "CrearDB -> " + mensaje, e);


        } finally {

            if (db != null)
                db.close();
        }
    }


    private static File dirApp() {

        File SDCardRoot = Environment.getExternalStorageDirectory(); //Environment.getDataDirectory(); //
        File dirApp = new File(SDCardRoot.getPath() + "/PruebaIngreso");

        if(!dirApp.isDirectory())
            dirApp.mkdirs();

        return dirApp;
    }

}
