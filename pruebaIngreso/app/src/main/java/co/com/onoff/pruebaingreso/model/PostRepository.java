package co.com.onoff.pruebaingreso.model;

import java.util.ArrayList;

public interface PostRepository {

    void getPostAPI();


    void savePost(ArrayList<Post> posts);
    void getPostDatabase();
    boolean validateDataOnDatabase();
    void getPostByTitle(String title);

}
