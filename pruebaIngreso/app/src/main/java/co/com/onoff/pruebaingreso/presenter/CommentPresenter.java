package co.com.onoff.pruebaingreso.presenter;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Comment;

public interface CommentPresenter {

    void getComments(String id);
    void saveComment(Comment comment);
    void  showComments(ArrayList<Comment> comments);
}
