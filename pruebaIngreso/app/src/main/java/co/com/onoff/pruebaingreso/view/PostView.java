package co.com.onoff.pruebaingreso.view;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.model.Post;

public interface PostView {

    /**
     * Metodo para obtener la informacion de los post
     */
    void getPost();

    /**
     * Metodo para retornar la info de los post y mostrarlos en pantalla
     * @param posts lista de post a mostrar en pantalla
     */
    void showPost(ArrayList<Post> posts);
}
