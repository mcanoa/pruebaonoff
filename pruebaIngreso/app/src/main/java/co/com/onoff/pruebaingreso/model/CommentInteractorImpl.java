package co.com.onoff.pruebaingreso.model;

import co.com.onoff.pruebaingreso.presenter.CommentPresenter;
import co.com.onoff.pruebaingreso.presenter.PostPresenter;

public class CommentInteractorImpl implements CommentInteractor{

    private CommentRepository commentRepository;
    public CommentInteractorImpl(CommentPresenter commentPresenter){

        commentRepository = new CommentRepositoryImpl(commentPresenter);
    }

    @Override
    public void getComments(String id) {

        commentRepository.getCommentsDatabae(id);
    }

    @Override
    public void saveComment(Comment comment) {
        commentRepository.saveComment(comment);
    }
}
