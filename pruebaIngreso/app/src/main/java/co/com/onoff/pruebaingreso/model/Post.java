package co.com.onoff.pruebaingreso.model;

import com.google.gson.JsonObject;

import java.io.Serializable;

public class Post implements Serializable {

    String userId, id, tittle, body;

    public Post(JsonObject postJson){
        userId = postJson.get(PropertiesJson.USERID).getAsString();
        id = postJson.get(PropertiesJson.ID_POST).getAsString();
        tittle = postJson.get(PropertiesJson.TITTLE).getAsString();
        body = postJson.get(PropertiesJson.BODY).getAsString();
    }

    public Post(String id, String title, String body){
        this.id=id;
        this.tittle=title;
        this.body=body;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
