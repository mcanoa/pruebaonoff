package co.com.onoff.pruebaingreso.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import co.com.onoff.pruebaingreso.R;
import co.com.onoff.pruebaingreso.model.Comment;
import co.com.onoff.pruebaingreso.model.Post;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {


    private final ArrayList<Comment> listComments;
    private final int resource;

    public CommentAdapter(ArrayList<Comment> listComments, int resource){
        this.listComments=listComments;
        this.resource= resource;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Comment comment= listComments.get(position);
        holder.setData(comment);
    }

    public void updateAdapter(Comment comment){

        listComments.add(comment);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return listComments.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        Comment comment;
        TextView body;
        public ViewHolder(View itemView) {
            super(itemView);

            body= itemView.findViewById(R.id.body);

        }

        public void setData(Comment comment){
            this.comment=comment;

            body.setText(comment.getComment());

        }

    }
}
