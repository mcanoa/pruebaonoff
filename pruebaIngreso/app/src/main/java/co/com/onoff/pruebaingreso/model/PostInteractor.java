package co.com.onoff.pruebaingreso.model;

public interface PostInteractor {

    void getPost();

    void getPostBy(String title);

}
